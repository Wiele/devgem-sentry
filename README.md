# DevGem.Sentry

[![Build status](https://gitlab.com/devgem/devgem-sentry/badges/master/build.svg)](https://gitlab.com/devgem/devgem-sentry/commits/master)

[![Quality Gate](http://inspect.devgem.be/api/badges/gate?key=devgem-sentry)](http://inspect.devgem.be/dashboard/index/devgem-sentry)
 [![Lines of Code](http://inspect.devgem.be/api/badges/measure?key=devgem-sentry&metric=ncloc)](http://inspect.devgem.be/component_measures?id=devgem-sentry&metric=ncloc)
 [![Technical Debt Ratio](http://inspect.devgem.be/api/badges/measure?key=devgem-sentry&metric=sqale_debt_ratio)](http://inspect.devgem.be/component_measures?id=devgem-sentry)


[![NuGet Version](https://img.shields.io/nuget/v/DevGem.Sentry.svg)](https://www.nuget.org/packages/Devgem.Sentry/)
 [![NuGet Downloads](https://img.shields.io/nuget/dt/DevGem.Sentry.svg)](https://www.nuget.org/packages/Devgem.Sentry/)


## Overview

.NET Core utility library for [`RavenSharp.Core`](https://www.nuget.org/packages/RavenSharp.Core/) containing ASP.NET Core filters and IoC to send exceptions to [`Sentry`](https://sentry.io).

## Usage

### Configuration

In `Startup.cs`:

```csharp
using DevGem.Sentry;
...
       public void ConfigureServices(IServiceCollection services)
       {
            // define the Sentry settings as constants or get them from your configuration
            // the environment and release arguments are optional
            services.AddSentry(YOUR_SENTRYDNS, YOUR_ENVIRONMENT, YOUR_RELEASE);
            ...
```
### Code

In some `controller`:

```csharp
using DevGem.Sentry;
...
    // if you put the SentryExceptionFilter attribute on a controller
    // then all exceptions thrown by the controller will be reported to Sentry
    [SentryExceptionFilter]
    public class SomeController : Controller
    {
      ...
```

## Get it!

You can clone and build DevGem.Sentry yourself, but for those of us who are happy with prebuilt binaries, there's a NuGet package: https://www.nuget.org/packages/Devgem.Sentry/.
