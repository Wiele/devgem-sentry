﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.Extensions.Logging;
using SharpRaven.Core;
using SharpRaven.Core.Data;

namespace DevGem.Sentry
{
    public class SentryExceptionFilterAttribute : TypeFilterAttribute
    {
        public SentryExceptionFilterAttribute() : base(typeof(SentryExceptionFilterAttributeImplementation))
        {
        }

        private class SentryExceptionFilterAttributeImplementation : IExceptionFilter
        {
            private readonly IRavenClient _ravenClient;
            private readonly ILoggerFactory _loggerFactory;

            public SentryExceptionFilterAttributeImplementation(IRavenClient ravenClient, ILoggerFactory loggerFactory) // NOSONAR
            {
                _ravenClient = ravenClient;
                _loggerFactory = loggerFactory;
            }

            public async void OnException(ExceptionContext context)
            {
                var eventId = await _ravenClient.CaptureAsync(new SentryEvent(context.Exception));
                var logger = _loggerFactory.CreateLogger(this.GetType().FullName);

                logger.LogError(
                    new EventId(0, eventId),
                    context.Exception,
                    context.Exception.GetType().FullName);
            }
        }
    }
}